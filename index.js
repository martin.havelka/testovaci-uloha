const express = require('express');
const morgan = require('morgan');

const config = require("./config");
const clients = require("./app/clients");
const contracts = require("./app/contracts");
const todos = require("./app/todos");


const app = express();

// enables request body parsing
app.use(express.json());

// enables simple request logging
app.use(morgan('tiny'));

// routes for client endpoints
app.get('/api/clients', clients.getAll);
app.get('/api/clients/:id', clients.getById);
app.post('/api/clients', clients.insert);
app.put('/api/clients/:id', clients.modify);
app.delete('/api/clients/:id', clients.delete);

app.get('/api/contractsForClient/:id', clients.getContracts);

// routes for contract endpoints

app.get('/api/contracts', contracts.getAll);
app.get('/api/contracts/:id', contracts.getById);
app.post('/api/contracts', contracts.insert);
app.put('/api/contracts/:id', contracts.modify);
app.delete('/api/contracts/:id', contracts.delete);

// route for todos

app.get('/api/todos', todos.todos);


// start server
app.listen(config.PORT, (err) => {
    if (err) console.log(err);
console.log(`listening on ${config.PORT}...`);
});