const mysql = require('mysql');
const config = require('../config');

// db connection setup
let connection = mysql.createConnection({
    host: config.mysql_host,
    user: config.mysql_user,
    password: config.mysql_pass,
    database: config.mysql_database,
    dateStrings: config.mysql_dateFormat   
});

// db connect
connection.connect((err) => {
    if (err) throw err;
    console.log("connected to database");
});

exports.con = connection;