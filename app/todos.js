const request = require("request");
const config = require("../config");

// calls external uri and returns response. success - 200; internal failure - 500
exports.todos = function(req, res) {
    request(config.todos_url, { json: true }, (err, response, body) => {
        if (err) { 
            res.status(500).send();
        }
        res.status(200).send(body);
      });
}