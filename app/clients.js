const database = require("./database"); 

// returns all clients and their data from database in a JSON array
// success - 200; internal failure - 500
exports.getAll = function(req, res) {
    let sqlQuery = `SELECT * FROM clients`;
    database.con.query(sqlQuery, (err, result) => {
        if (err) {
            console.log(err); 
            res.status(500).send();
        }
        res.status(200).json(result);
    });
}

// returns client's data as JSON object by unique id
// success - 200; not found - 404; internal failure - 500
exports.getById = function(req, res) {
    let sqlQuery = `SELECT * FROM clients WHERE id=${req.params.id}`;
    database.con.query(sqlQuery, (err, result) => {
        if (err) {
            console.log(err); 
            res.status(500).send();
        } 
        if (result.length != 0) {
            res.status(200).json(result);
        } else {
            res.status(404).send();
        }
        
    });
}

// creates new client in database
// data comes in JSON object in request body
// success - 201; internal failure - 500
exports.insert = function(req, res) {
    let client = req.body;
    let sqlQuery = `INSERT INTO clients (firstName, lastName, birthDate) VALUES ("${client.firstName}", "${client.lastName}", "${client.birthDate}")`;
    database.con.query(sqlQuery, (err, result) => {
        console.log(result);
        if (err) {
            console.log(err); 
            res.status(500).send();
        } 
        client.id = result.insertId;
        res.status(201).send(client);
    });
}

// modifies user by his id
// data sent as object in body
// success - 201; not found - 404; internal failure - 500
exports.modify = function(req, res) {
    let client = req.body;
    let sqlQuery = `UPDATE clients SET firstName = "${client.firstName}", lastName = "${client.lastName}", birthDate = "${client.birthDate}" WHERE id = ${req.params.id}`;

    database.con.query(sqlQuery, (err, result) => {
        if (err) {
            console.log(err); 
            res.status(500).send();
        } else if (result.affectedRows == 0) {
            res.status(404).send();    
        } 
        client.id = req.params.id;
        res.status(201).send(client);
    });
}
// deletes user by his id
// success - 204; not found - 404; internal failure - 500
exports.delete = function(req, res) {
    let sqlQuery = `DELETE FROM clients WHERE id=${req.params.id}`;
    database.con.query(sqlQuery, (err, result) => {
        if (err) {
            console.log(err); 
            res.status(500).send();
        } 

        if (result.affectedRows == 1) {
            res.status(204).send();    
        }
        res.status(404).send();
    });
}

exports.getContracts = function(req, res) {
    let userQuery = `SELECT * FROM clients WHERE id=${req.params.id}`;
    let contractQuery = `SELECT id, contractName, payment FROM contracts WHERE clientId = ${req.params.id}`;
    database.con.query(userQuery, (err, user) => {
        if (err) {
            console.log(err); 
            res.status(500).send();
        } 
        if (user.length == 0) {
            res.status(404).send();
        } else {
            database.con.query(contractQuery, (err, contracts) => {
                if (err) {
                    console.log(err); 
                    res.status(500).send();
                }
                user[0].contracts = contracts;
                res.status(200).json(user);
            });
        }
        
    });    
}