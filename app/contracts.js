const database = require("./database"); 

// returns all contracts and their data from database in a JSON array
// success - 200; internal failure - 500
exports.getAll = function(req,res) {
    let sqlQuery = `SELECT * FROM contracts`;
    database.con.query(sqlQuery, (err, result) => {
        if (err) {
            console.log(err); 
            res.status(500).send();
        }
        res.status(200).send(result);
    });
}

// returns contract data as JSON object by unique id
// success - 200; not found - 404; internal failure - 500
exports.getById = function(req,res) {
    let sqlQuery = `SELECT * FROM contracts WHERE id=${req.params.id}`;
    database.con.query(sqlQuery, (err, result) => {
        if (err) {
            console.log(err); 
            res.status(500).send();
        } 
        if (result.length != 0) {
            res.status(200).send(result);
        } else {
            res.status(404).send();
        }
        
    });
}

// creates new contract in database
// data comes in JSON object in request body
// success - 201; internal failure - 500
exports.insert = function(req,res) {
    let contract = req.body;
    let sqlQuery = `INSERT INTO contracts (contractName, payment, clientId) VALUES ("${contract.contractName}", "${contract.payment}", "${contract.clientId}")`;
    database.con.query(sqlQuery, (err, result) => {
        if (err) {
            console.log(err); 
            res.status(500).send();
        } 
        contract.id = result.insertId;
        res.status(201).send(contract);
    });
}

// modifies contract by its id
// data sent as object in body
// success - 201; not found - 404; internal failure - 500
exports.modify = function(req,res) {
    let contract = req.body;
    let sqlQuery = `UPDATE contracts SET contractName = "${contract.contractName}", payment = "${contract.payment}", clientId = "${contract.clientId}" WHERE id = ${req.params.id}`;
    database.con.query(sqlQuery, (err, result) => {
        if (err) {
            console.log(err); 
            res.status(500).send();
        } else if (result.affectedRows == 0) {
            res.status(404).send();    
        } 
        contract.id = req.params.id;
        res.status(201).send(contract);
    });
}
// deletes contract by his id
// success - 204; not found - 404; internal failure - 500
exports.delete = function(req,res) {
    let sqlQuery = `DELETE FROM contracts WHERE id=${req.params.id}`;
    database.con.query(sqlQuery, (err, result) => {
        if (err) {
            console.log(err); 
            res.status(500).send();
        } 
        
        if (result.affectedRows == 1) {
            res.status(204).send();    
        }
        res.status(404).send();
    });
}