const config = {};


config.PORT = process.env.PORT || 3000;

config.mysql_host = process.env.MYSQL_HOST || "localhost";
config.mysql_user = process.env.MYSQL_USER || "backend";
config.mysql_pass = process.env.MYSQL_PASS || "password";
config.mysql_database = process.env.MYSQL_DATABASE || "nodejsTest";
config.mysql_dateFormat = process.env.MYSQL_DATEFORMAT || "date";

config.todos_url = process.env.TODOS_URL || "https://jsonplaceholder.typicode.com/todos";



module.exports = config;