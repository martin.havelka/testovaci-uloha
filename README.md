# Testovací úloha

- jednoduchý backend pro testovací účely. Provádí CRUD operace nad MySQL databází.
- původní zadání, které zároveň dokumentuje endpointy je součástí repozitáře  

## Spuštění serveru

prerekvizity:
 - nodejs 
 - mysql databáze podle zadání
----------------
1. instalace npm balíčků:
```bash
npm i
```


2. spuštění:
```bash
npm start
```
## Struktura aplikace

vstupní soubor: index.js

 - definice endpointů a dalších middleware

samotné handler funkce pro endpointy jsou pak rozděleny do 3 souborů:
- `./app/clients.js`
- `./app/contracts.js`
- `./app/todos.js`

konfigurace se nastavuje v souboru `./config.js`

## Poznámka MHa k okolnostem a stylu vývoje

Rád bych kromě samotného kódu ještě zmínil myšlenku za několika věcmi. Po čtení zadání jsem se sice rozhodl o způsobu psaní aplikace tak, aby byla logicky rozčleněná do modulů a co nejlépe čitelná a čistá, ale zároveň mě napadlo, že kdybych měl vážně psát podobnou aplikaci v prostředí, kde záleží na čase, neměl bych to přehánět se scalabilitou, čitelností atd. Proto jsem se pokusil čistotu a efektivitu psaní nějak zkloubit do sebe, aby to co nejvíc vypovídalo tomu, jak bych se k problému postavil.

Kdyby se ale jednalo o aplikaci s predispozicemi k většimu růstu, určitě bych přidal:

- Unit testy
- efektivnější a znovupoužitelnější kód pro komunikaci s databází - contracts a clients jsou prakticky to samé, více proměnných v config než hard-coded
- lepší logování
- lepší error handling - najít/napsat adekvátní middleware
- použití frameworku :)
